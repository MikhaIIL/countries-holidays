package com.manufriev.example.countriesholidays.api

import com.manufriev.example.countriesholidays.responses.CountryDto
import com.manufriev.example.countriesholidays.responses.CountryInfoDto
import com.manufriev.example.countriesholidays.responses.HolidayDto
import retrofit2.http.GET
import retrofit2.http.Path

interface MainApi {

    @GET("/api/v3/AvailableCountries")
    suspend fun getCountries(): List<CountryDto>

    @GET("/api/v3/PublicHolidays/{year}/{countryCode}")
    suspend fun getHolidays(@Path("year") year: Int,
                            @Path("countryCode") countryCode: String): List<HolidayDto>

    @GET("/api/v3/CountryInfo/{countryCode}")
    suspend fun getCountryInfo(@Path("countryCode") countryCode: String): CountryInfoDto

}