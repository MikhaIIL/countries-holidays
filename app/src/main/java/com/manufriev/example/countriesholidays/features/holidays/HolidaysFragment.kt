package com.manufriev.example.countriesholidays.features.holidays

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.manufriev.example.countriesholidays.MainActivity
import com.manufriev.example.countriesholidays.R
import com.manufriev.example.countriesholidays.databinding.FragmentHolidaysLayoutBinding
import com.manufriev.example.countriesholidays.extentions.isConnectionAvailable
import com.manufriev.example.countriesholidays.extentions.showToastLong
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@ExperimentalCoroutinesApi
class HolidaysFragment: Fragment(R.layout.fragment_holidays_layout) {

    private lateinit var viewModel: CountryHolidaysViewModel

    private var _binding: FragmentHolidaysLayoutBinding? = null
    private val binding
        get() = _binding!!

    private val adapter = HolidaysRecyclerAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = (requireActivity() as MainActivity).viewModelFactory.create(CountryHolidaysViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentHolidaysLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val countryCode = requireArguments().getString(CODE_ARG_KEY) ?: "RU"
        initViews()
        initObservers()
        viewModel.setCountryCode(countryCode)
        if (isConnectionAvailable()) viewModel.refresh()
    }

    private fun initViews() {
        binding.holidaysRecycler.adapter = adapter
    }

    private fun initObservers() {
        viewModel.holidays.onEach {
            adapter.items = it
        }.catch {
            showToastLong(R.string.load_data_error_message)
        }.launchIn(lifecycleScope)
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }


    companion object {
        private const val CODE_ARG_KEY = "code"

        fun getInstance(countryCode: String): HolidaysFragment {
            return HolidaysFragment().apply {
                arguments = Bundle().apply {
                    this.putString(CODE_ARG_KEY, countryCode)
                }
            }
        }
    }
}