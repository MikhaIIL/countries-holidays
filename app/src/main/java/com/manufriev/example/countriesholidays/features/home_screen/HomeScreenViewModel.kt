package com.manufriev.example.countriesholidays.features.home_screen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.manufriev.example.countriesholidays.repositories.HolidaysRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch


class HomeScreenViewModel(private val repository: HolidaysRepository) : ViewModel() {

    private val searchQuery = MutableStateFlow("")

    val countriesList = repository.getCountries(searchQuery)

    fun refresh() = viewModelScope.launch(Dispatchers.IO) {
        repository.refreshCountries()
    }

    fun setQuery(query: String) {
        searchQuery.value = query
    }
}