package com.manufriev.example.countriesholidays.datasources.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.manufriev.example.countriesholidays.responses.CountryDto

@Entity(tableName = "country")
data class CountryEntity(
    @PrimaryKey
    val code: String,
    val name: String
) {
    companion object {
        fun fromDto (dto: CountryDto): CountryEntity {
            return CountryEntity(dto.countryCode, dto.countryName)
        }
    }
}