package com.manufriev.example.countriesholidays.datasources.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.manufriev.example.countriesholidays.responses.HolidayDto
import org.joda.time.DateTime

@Entity(tableName = "Holidays", primaryKeys = ["code", "date"])
@TypeConverters(DateConverter::class)
data class HolidayEntity(
    val code: String,
    val year: Int,
    @TypeConverters(DateConverter::class)
    val date: DateTime,
    val localName: String,
    val name: String,
    val launchYear: Int
) {
    companion object {
        fun fromDto (dto: HolidayDto): HolidayEntity {
            val date = DateTime.parse(dto.date)
            return HolidayEntity(
                code = dto.countryCode,
                year = date.year,
                date = date,
                localName = dto.localName,
                name = dto.name,
                launchYear = dto.launchYear)
        }
    }
}


class DateConverter {

    @TypeConverter
    fun toDate(timestamp: Long): DateTime {
        return DateTime(timestamp)
    }

    @TypeConverter
    fun toTimestamp(date: DateTime): Long {
        return date.millis
    }
}