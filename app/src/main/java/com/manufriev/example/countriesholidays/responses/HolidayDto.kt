package com.manufriev.example.countriesholidays.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import org.joda.time.DateTime

@JsonIgnoreProperties(ignoreUnknown = true)
data class HolidayDto (
    @JsonProperty("date")
    val date: String,
    @JsonProperty("localName")
    val localName: String,
    @JsonProperty("name")
    val name: String,
    @JsonProperty("countryCode")
    val countryCode: String,
    @JsonProperty("launchYear")
    val launchYear: Int)