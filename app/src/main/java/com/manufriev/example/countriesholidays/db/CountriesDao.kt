package com.manufriev.example.countriesholidays.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.manufriev.example.countriesholidays.datasources.entities.CountryEntity
import com.manufriev.example.countriesholidays.datasources.entities.CountryInfoEntity
import com.manufriev.example.countriesholidays.datasources.entities.FavoriteEntity
import com.manufriev.example.countriesholidays.datasources.entities.HolidayEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface CountriesDao {

    @Query("SELECT * FROM country")
    fun getCountries(): Flow<List<CountryEntity>>

    @Query("SELECT * FROM country WHERE code in (:codes)")
    fun getCountries(codes: List<String>): List<CountryEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCommonCountries(countries: List<CountryEntity>)

    @Query("SELECT * FROM country_info WHERE code = :code")
    fun getInfoByCountry(code: String): Flow<CountryInfoEntity?>

    @Query("SELECT * FROM country_info WHERE code IN (:codes)")
    fun getInfoByCountry(codes: List<String>): Flow<List<CountryInfoEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertInfo(entity: CountryInfoEntity)

    @Query("SELECT * FROM favorites_countries")
    fun getFavoritesCountries(): Flow<List<FavoriteEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addFavorite(entity: FavoriteEntity)

    @Query("DELETE FROM favorites_countries WHERE code = :code")
    fun removeFromFavorite(code: String)

    @Query("select count(*) FROM favorites_countries WHERE code = :code")
    fun containsFavorite(code: String): Boolean

    @Query("SELECT * FROM holidays WHERE code = :code AND year = :year")
    fun getHolidays(code: String, year: Int): Flow<List<HolidayEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertHolidays(entities: List<HolidayEntity>)
}