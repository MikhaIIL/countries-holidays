package com.manufriev.example.countriesholidays.datasources.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.manufriev.example.countriesholidays.responses.CountryInfoDto

@Entity(tableName = "country_info")
@TypeConverters(BorderCountriesConverter::class)
data class CountryInfoEntity(
    @PrimaryKey
    val code: String,
    val commonName: String,
    val officialName: String,
    val region: String,
    @TypeConverters(BorderCountriesConverter::class)
    val borders: List<String>,
    val isFavorite: Boolean = false
) {
    companion object {
        fun fromDto (dto: CountryInfoDto): CountryInfoEntity {
            return CountryInfoEntity(
                code = dto.countryCode,
                commonName = dto.commonName,
                officialName = dto.officialName,
                region = dto.region,
                borders = dto.borders?.map { fromDto(it).code } ?: emptyList())
        }
    }
}

class BorderCountriesConverter {

    @TypeConverter
    fun toList(countries: String): List<String> {
        return countries.split(" ").mapNotNull { it }
    }

    @TypeConverter
    fun toString(countries: List<String>): String {
        return countries.joinToString(" ")
    }
}


@Entity(tableName = "favorites_countries")
data class FavoriteEntity(@PrimaryKey val code: String)