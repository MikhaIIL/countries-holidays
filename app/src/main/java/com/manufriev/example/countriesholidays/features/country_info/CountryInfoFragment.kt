package com.manufriev.example.countriesholidays.features.country_info

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.manufriev.example.countriesholidays.MainActivity
import com.manufriev.example.countriesholidays.R
import com.manufriev.example.countriesholidays.databinding.FragmentCountryInfoLayoutBinding
import com.manufriev.example.countriesholidays.extentions.isConnectionAvailable
import com.manufriev.example.countriesholidays.extentions.showToastLong
import com.manufriev.example.countriesholidays.features.holidays.HolidaysFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@ExperimentalCoroutinesApi
class CountryInfoFragment: Fragment(R.layout.fragment_country_info_layout) {

    private lateinit var viewModel: CountryInfoViewModel

    private var _binding: FragmentCountryInfoLayoutBinding? = null
    private val binding
        get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = (requireActivity() as MainActivity).viewModelFactory.create(CountryInfoViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentCountryInfoLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val countryCode = requireArguments().getString(CODE_ARG_KEY) ?: "RU"

        binding.makeFavorite.setOnClickListener { viewModel.makeFavorite() }
        binding.showHolidaysBtn.setOnClickListener { processShowHolidaysClick(countryCode) }

        viewModel.observableInfo.onEach { country ->
            binding.commonName.text = country.commonName
            binding.officialName.text = country.officialName
            binding.region.text = country.region
            binding.borderCountries.text = country.borders.joinToString(", ") { it.name }
            with(binding.makeFavorite) {
                val drawableId = if (country.isFavorite) R.drawable.ic_baseline_star_24
                else R.drawable.ic_baseline_star_outline_24
                setImageDrawable(ContextCompat.getDrawable(context, drawableId))
            }
        }.catch {
            showToastLong(R.string.load_data_error_message)
        }.launchIn(lifecycleScope)

        viewModel.setCountryCode(countryCode)
        if (isConnectionAvailable()) viewModel.refresh()
    }

    private fun processShowHolidaysClick(code: String) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container_view, HolidaysFragment.getInstance(code))
            .addToBackStack(HolidaysFragment.toString())
            .commit()
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {
        private const val CODE_ARG_KEY = "code"

        fun getInstance(countryCode: String): CountryInfoFragment {
            return CountryInfoFragment().apply {
                arguments = Bundle().apply {
                    this.putString(CODE_ARG_KEY, countryCode)
                }
            }
        }
    }
}