package com.manufriev.example.countriesholidays.repositories

import com.manufriev.example.countriesholidays.datasources.cache.CountriesCacheDatasource
import com.manufriev.example.countriesholidays.datasources.entities.CountryEntity
import com.manufriev.example.countriesholidays.datasources.entities.CountryInfoEntity
import com.manufriev.example.countriesholidays.datasources.entities.HolidayEntity
import com.manufriev.example.countriesholidays.datasources.remote.HolidaysRemoteDatasource
import com.manufriev.example.countriesholidays.vo.Country
import com.manufriev.example.countriesholidays.vo.CountryInfo
import com.manufriev.example.countriesholidays.vo.Holiday
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import org.joda.time.DateTime

@ExperimentalCoroutinesApi
class HolidaysRepository(private val cacheSource: CountriesCacheDatasource) {
    private val remoteDataSource = HolidaysRemoteDatasource

    fun getCountries(searchQuery: Flow<String>): Flow<List<Country>> {
        return cacheSource.getCountries().combine(searchQuery) { items, query ->
            items.filter { it.name.contains(query, ignoreCase = true) }
                .map { Country.fromEntity(it) }
        }
    }

    suspend fun refreshCountries() {
        val remoteData = remoteDataSource.getCountries()
        cacheSource.insertCommonCountries(remoteData.map { CountryEntity.fromDto(it) })
    }

    fun getCountryInfo(code: String): Flow<CountryInfo> {
        return cacheSource.getCountryInfo(code).combine(cacheSource.getFavorites()) { country, favs ->
            if (country == null) null
            else CountryInfo.fromEntity(country).apply {
                borders = cacheSource.getCountries(country.borders).map {
                    Country.fromEntity(it)
                }
                isFavorite = favs.contains(code)
            }
        }.filterNotNull().flowOn(Dispatchers.IO)
    }

    suspend fun refreshInfo(code: String) {
        val remoteInfo = remoteDataSource.getCountryInfo(code)
        cacheSource.insertInfo(CountryInfoEntity.fromDto(remoteInfo))
    }

    fun getHolidays(countryCode: String): Flow<List<Holiday>> {
        val requestYear = DateTime.now().year
        return cacheSource.getHolidays(countryCode, requestYear).map { holidays ->
            holidays.map { Holiday.fromEntity(it) }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun refreshHolidays(countryCode: String) {
        val requestYear = DateTime.now().year
        val remoteInfo = remoteDataSource.getHolidays(requestYear, countryCode)
        cacheSource.insertHolidays(remoteInfo.map { HolidayEntity.fromDto(it) })
    }

    fun getFavorites(): Flow<List<CountryInfo>> {
        return cacheSource.getFavorites().flatMapLatest { codes ->
            cacheSource.getCountriesInfo(codes).map { entities ->
                entities.map { CountryInfo.fromEntity(it) }
            }
        }
    }

    fun makeFavorite(code: String) {
        cacheSource.changeFavorite(code)
    }
}