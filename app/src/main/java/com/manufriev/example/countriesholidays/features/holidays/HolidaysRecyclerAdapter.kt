package com.manufriev.example.countriesholidays.features.holidays

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.manufriev.example.countriesholidays.R
import com.manufriev.example.countriesholidays.databinding.ItemHolidayLayoutBinding
import com.manufriev.example.countriesholidays.features.BaseRecyclerAdapter
import com.manufriev.example.countriesholidays.vo.Holiday

class HolidaysRecyclerAdapter: BaseRecyclerAdapter<Holiday>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_holiday_layout, parent, false)
        return HolidayViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position < 0 || holder !is HolidayViewHolder) return

        holder.binding.name.text = items[position].name
        holder.binding.localName.text = items[position].localName
        holder.binding.date.text = items[position].date.toString("dd-MM-yyyy")
        holder.binding.launchYear.text = items[position].launchYear.toString()
    }

    inner class HolidayViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val binding = ItemHolidayLayoutBinding.bind(itemView)
    }
}