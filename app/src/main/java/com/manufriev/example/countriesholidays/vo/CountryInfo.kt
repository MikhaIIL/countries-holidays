package com.manufriev.example.countriesholidays.vo

import com.manufriev.example.countriesholidays.datasources.entities.CountryInfoEntity
import com.manufriev.example.countriesholidays.features.Diffable

data class CountryInfo (
    val commonName: String,
    val officialName: String,
    val code: String,
    val region: String,
    var borders: List<Country> = emptyList(),
    var isFavorite: Boolean = false): Diffable<CountryInfo> {

    override fun areItemsTheSame(new: CountryInfo): Boolean {
        return code == new.code
    }

    override fun areContentsTheSame(new: CountryInfo): Boolean {
        return commonName == new.commonName && officialName == new.officialName &&
                region == new.region && borders == new.borders && isFavorite == new.isFavorite
    }

    companion object {
        fun fromEntity (entity: CountryInfoEntity): CountryInfo {
            return CountryInfo(
                commonName = entity.commonName,
                officialName = entity.officialName,
                code = entity.code,
                region = entity.region,
                isFavorite = entity.isFavorite,
                borders = emptyList()
            )
        }
    }
}