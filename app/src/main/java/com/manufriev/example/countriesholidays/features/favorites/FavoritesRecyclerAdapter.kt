package com.manufriev.example.countriesholidays.features.favorites

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.manufriev.example.countriesholidays.R
import com.manufriev.example.countriesholidays.databinding.ItemFavoriteCountryLayoutBinding
import com.manufriev.example.countriesholidays.features.BaseRecyclerAdapter
import com.manufriev.example.countriesholidays.vo.CountryInfo

class FavoritesRecyclerAdapter(private val onItemClicked: (code: String) -> Unit): BaseRecyclerAdapter<CountryInfo>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_favorite_country_layout, parent, false)
        return FavoriteCountryViewHolder(view, this::processItemClick)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position < 0 || holder !is FavoriteCountryViewHolder) return

        holder.binding.countryName.text = items[position].commonName
        holder.binding.regionName.text = items[position].region
    }

    private fun processItemClick(pos: Int) {
        if (pos >= 0) {
            onItemClicked.invoke(items[pos].code)
        }
    }

    inner class FavoriteCountryViewHolder(itemView: View, onItemClick: (pos: Int) -> Unit): RecyclerView.ViewHolder(itemView) {
        val binding = ItemFavoriteCountryLayoutBinding.bind(itemView)
        init {
            itemView.setOnClickListener {
                onItemClick.invoke(adapterPosition)
            }
        }
    }
}