package com.manufriev.example.countriesholidays.vo

import com.manufriev.example.countriesholidays.datasources.entities.CountryEntity
import com.manufriev.example.countriesholidays.features.Diffable

data class Country (val code: String, val name: String): Diffable<Country> {

    override fun areItemsTheSame(new: Country): Boolean {
        return code == new.code
    }

    override fun areContentsTheSame(new: Country): Boolean {
        return name == new.name
    }

    companion object {
        fun fromEntity (entity: CountryEntity): Country {
            return Country(entity.code, entity.name)
        }
    }
}