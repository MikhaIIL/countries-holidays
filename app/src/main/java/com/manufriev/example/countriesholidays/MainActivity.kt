package com.manufriev.example.countriesholidays

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.manufriev.example.countriesholidays.datasources.cache.CountriesCacheDatasource
import com.manufriev.example.countriesholidays.db.AppDatabase
import com.manufriev.example.countriesholidays.features.home_screen.HomeScreenFragment
import com.manufriev.example.countriesholidays.repositories.HolidaysRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class MainActivity : AppCompatActivity() {
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dao = AppDatabase.getInstance(application).countriesDao
        val repository = HolidaysRepository(CountriesCacheDatasource(dao))
        viewModelFactory = ViewModelFactory(repository)

        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_container_view, HomeScreenFragment())
            .addToBackStack(HomeScreenFragment::class.java.name)
            .commit()
    }
}