package com.manufriev.example.countriesholidays.features.home_screen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.manufriev.example.countriesholidays.R
import com.manufriev.example.countriesholidays.databinding.ItemCountryLayoutBinding
import com.manufriev.example.countriesholidays.features.BaseRecyclerAdapter
import com.manufriev.example.countriesholidays.vo.Country

class CountriesRecyclerAdapter(private val onItemClicked: (code: String) -> Unit): BaseRecyclerAdapter<Country>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_country_layout, parent, false)
        return CountryViewHolder(view, this::processItemClick)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position < 0 || holder !is CountryViewHolder) return

        holder.binding.countryName.text = items[position].name
    }

    private fun processItemClick(pos: Int) {
        if (pos >= 0) {
            onItemClicked.invoke(items[pos].code)
        }
    }

    inner class CountryViewHolder(itemView: View, onItemClick: (pos: Int) -> Unit): RecyclerView.ViewHolder(itemView) {
        val binding = ItemCountryLayoutBinding.bind(itemView)
        init {
            itemView.setOnClickListener {
                onItemClick.invoke(adapterPosition)
            }
        }
    }
}