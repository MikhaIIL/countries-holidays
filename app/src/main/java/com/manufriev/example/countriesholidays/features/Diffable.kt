package com.manufriev.example.countriesholidays.features

interface Diffable<T> {

    fun areItemsTheSame(new: T): Boolean

    fun areContentsTheSame(new: T): Boolean
}