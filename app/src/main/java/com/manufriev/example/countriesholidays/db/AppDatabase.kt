package com.manufriev.example.countriesholidays.db

import android.content.Context
import androidx.room.RoomDatabase
import androidx.room.Database
import com.manufriev.example.countriesholidays.datasources.entities.CountryEntity
import androidx.room.Room
import com.manufriev.example.countriesholidays.datasources.entities.CountryInfoEntity
import com.manufriev.example.countriesholidays.datasources.entities.FavoriteEntity
import com.manufriev.example.countriesholidays.datasources.entities.HolidayEntity


@Database(
    entities = [CountryEntity::class, CountryInfoEntity::class, FavoriteEntity::class, HolidayEntity::class],
    version = 2)
abstract class AppDatabase : RoomDatabase() {
    abstract val countriesDao: CountriesDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "holidays_database"
                    ).build()
                }
                return instance
            }
        }
    }
}