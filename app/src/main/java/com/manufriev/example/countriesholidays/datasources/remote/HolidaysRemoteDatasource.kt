package com.manufriev.example.countriesholidays.datasources.remote

import com.fasterxml.jackson.databind.ObjectMapper
import com.manufriev.example.countriesholidays.api.MainApi
import com.manufriev.example.countriesholidays.responses.CountryDto
import com.manufriev.example.countriesholidays.responses.CountryInfoDto
import com.manufriev.example.countriesholidays.responses.HolidayDto
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit

object HolidaysRemoteDatasource {

    private const val BASE_URL = "https://date.nager.at"
    private const val BASE_REQUEST_TIMEOUT = 45L

    private val okHttpClient = getOkHttpClient()
    private val retrofit = getRetrofit()
    private val mainApi: MainApi = retrofit.create(MainApi::class.java)

    suspend fun getCountries(): List<CountryDto> {
        return mainApi.getCountries()
    }

    suspend fun getHolidays(year: Int, countryCode: String): List<HolidayDto> {
        return mainApi.getHolidays(year, countryCode)
    }

    suspend fun getCountryInfo(countryCode: String): CountryInfoDto {
        return mainApi.getCountryInfo(countryCode)
    }

    private fun getOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(BASE_REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(BASE_REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .build()
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(JacksonConverterFactory.create(ObjectMapper()))
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .build()
    }
}