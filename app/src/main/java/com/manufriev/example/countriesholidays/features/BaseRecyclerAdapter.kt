package com.manufriev.example.countriesholidays.features

import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter<R : Diffable<R>> : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val differ = AsyncListDiffer(this, Differ<R>())

    var items: List<R>
        get() = differ.currentList
        set(value) = differ.submitList(value)

    fun setItems(items: List<R>, submitCallback: () -> Unit) {
        differ.submitList(items, submitCallback)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private class Differ<R : Diffable<R>> : DiffUtil.ItemCallback<R>() {
        override fun areItemsTheSame(oldItem: R, newItem: R): Boolean {
            return oldItem.areItemsTheSame(newItem)
        }

        override fun areContentsTheSame(oldItem: R, newItem: R): Boolean {
            return oldItem.areContentsTheSame(newItem)
        }
    }
}