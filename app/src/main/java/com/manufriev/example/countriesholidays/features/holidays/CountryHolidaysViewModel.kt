package com.manufriev.example.countriesholidays.features.holidays

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.manufriev.example.countriesholidays.repositories.HolidaysRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch


@ExperimentalCoroutinesApi
class CountryHolidaysViewModel(private val repository: HolidaysRepository) : ViewModel() {

    private val countryCode = MutableStateFlow("")
    val holidays = countryCode.filter { it.isNotBlank() }.flatMapLatest {
        repository.getHolidays(it)
    }

    fun setCountryCode(code: String) = viewModelScope.launch(Dispatchers.IO) {
        countryCode.value = code
    }

    fun refresh() = viewModelScope.launch(Dispatchers.IO) {
        repository.refreshHolidays(countryCode.value)
    }
}