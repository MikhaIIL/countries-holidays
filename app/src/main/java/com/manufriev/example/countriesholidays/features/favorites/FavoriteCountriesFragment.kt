package com.manufriev.example.countriesholidays.features.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.manufriev.example.countriesholidays.MainActivity
import com.manufriev.example.countriesholidays.R
import com.manufriev.example.countriesholidays.databinding.FragmentFavoritesCountriesLayoutBinding
import com.manufriev.example.countriesholidays.features.country_info.CountryInfoFragment
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class FavoriteCountriesFragment: Fragment(R.layout.fragment_favorites_countries_layout) {

    private lateinit var viewModel: FavoritesCountriesViewModel

    private var _binding: FragmentFavoritesCountriesLayoutBinding? = null
    private val binding
        get() = _binding!!

    private val adapter = FavoritesRecyclerAdapter(this::processCountryClick)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = (requireActivity() as MainActivity).viewModelFactory.create(FavoritesCountriesViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentFavoritesCountriesLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
    }

    private fun initViews() {
        binding.countriesRecycler.adapter = adapter
    }

    private fun initObservers() {
        viewModel.countriesList.onEach {
            adapter.items = it
        }.catch {
            Toast.makeText(context, "Unable load data", Toast.LENGTH_LONG).show()
        }.launchIn(lifecycleScope)
    }

    private fun processCountryClick(code: String) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container_view, CountryInfoFragment.getInstance(code))
            .addToBackStack(CountryInfoFragment.toString())
            .commit()
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}