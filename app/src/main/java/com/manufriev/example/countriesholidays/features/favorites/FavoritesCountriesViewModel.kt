package com.manufriev.example.countriesholidays.features.favorites

import androidx.lifecycle.ViewModel
import com.manufriev.example.countriesholidays.repositories.HolidaysRepository


class FavoritesCountriesViewModel(private val repository: HolidaysRepository) : ViewModel() {

    val countriesList = repository.getFavorites()
}