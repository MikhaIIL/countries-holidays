package com.manufriev.example.countriesholidays.vo

import com.manufriev.example.countriesholidays.datasources.entities.HolidayEntity
import com.manufriev.example.countriesholidays.features.Diffable
import org.joda.time.DateTime

data class Holiday (
    val code: String,
    val date: DateTime,
    val localName: String,
    val name: String,
    val launchYear: Int): Diffable<Holiday> {

    override fun areItemsTheSame(new: Holiday): Boolean {
        return code == new.code
    }

    override fun areContentsTheSame(new: Holiday): Boolean {
        return name == new.name && localName == new.localName &&
                date == new.date && launchYear == new.launchYear
    }

    companion object {
        fun fromEntity (entity: HolidayEntity): Holiday {
            return Holiday(entity.code, entity.date, entity.name, entity.localName, entity.launchYear)
        }
    }
}