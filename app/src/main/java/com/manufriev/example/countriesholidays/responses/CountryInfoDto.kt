package com.manufriev.example.countriesholidays.responses

import com.fasterxml.jackson.annotation.JsonProperty

data class CountryInfoDto (
    @JsonProperty("commonName")
    val commonName: String,
    @JsonProperty("officialName")
    val officialName: String,
    @JsonProperty("countryCode")
    val countryCode: String,
    @JsonProperty("region")
    val region: String,
    @JsonProperty("borders")
    val borders: List<CountryInfoDto>?
        )