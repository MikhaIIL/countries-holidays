package com.manufriev.example.countriesholidays.features.country_info

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.manufriev.example.countriesholidays.repositories.HolidaysRepository
import com.manufriev.example.countriesholidays.vo.CountryInfo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch


@ExperimentalCoroutinesApi
class CountryInfoViewModel(private val repository: HolidaysRepository) : ViewModel() {
    private val countryCode = MutableStateFlow<String?>(null)

    private val _observableInfo = countryCode.filterNotNull().flatMapLatest {
        repository.getCountryInfo(it)
    }
    val observableInfo: Flow<CountryInfo>
        get() = _observableInfo.filterNotNull()

    fun setCountryCode(code: String) = viewModelScope.launch(Dispatchers.IO) {
        countryCode.value = code
    }

    fun refresh() = viewModelScope.launch(Dispatchers.IO) {
        countryCode.value?.let {
            repository.refreshInfo(it)
        }
    }

    fun makeFavorite() = viewModelScope.launch(Dispatchers.IO) {
        repository.makeFavorite(countryCode.value ?: "")
    }
}