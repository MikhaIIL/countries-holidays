package com.manufriev.example.countriesholidays.features.home_screen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.manufriev.example.countriesholidays.MainActivity
import com.manufriev.example.countriesholidays.R
import com.manufriev.example.countriesholidays.databinding.FragmentHomeScreenLayoutBinding
import com.manufriev.example.countriesholidays.extentions.isConnectionAvailable
import com.manufriev.example.countriesholidays.extentions.showToastLong
import com.manufriev.example.countriesholidays.features.country_info.CountryInfoFragment
import com.manufriev.example.countriesholidays.features.favorites.FavoriteCountriesFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@ExperimentalCoroutinesApi
class HomeScreenFragment: Fragment(R.layout.fragment_home_screen_layout) {

    private lateinit var viewModel: HomeScreenViewModel

    private var _binding: FragmentHomeScreenLayoutBinding? = null
    private val binding
        get() = _binding!!

    private val adapter = CountriesRecyclerAdapter(this::processCountryClick)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = (requireActivity() as MainActivity).viewModelFactory.create(HomeScreenViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentHomeScreenLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
        if (isConnectionAvailable()) viewModel.refresh()
    }

    private fun initViews() {
        binding.swipeRefresh.setOnRefreshListener { viewModel.refresh() }

        binding.searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.setQuery(newText ?: "")
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.setQuery(query ?: "")
                return true
            }
        })
        binding.countriesRecycler.adapter = adapter
        binding.toolbar.setOnMenuItemClickListener {
            if (it.itemId == R.id.favorites_item) processFavoritesClick()
            true
        }
    }

    private fun initObservers() {
        viewModel.countriesList.onEach {
            binding.swipeRefresh.isRefreshing = false
            adapter.items = it
        }.catch {
            showToastLong(R.string.load_data_error_message)
        }.launchIn(lifecycleScope)
    }

    private fun processCountryClick(code: String) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container_view, CountryInfoFragment.getInstance(code))
            .addToBackStack(CountryInfoFragment.toString())
            .commit()
    }

    private fun processFavoritesClick() {
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container_view, FavoriteCountriesFragment())
            .addToBackStack(FavoriteCountriesFragment::class.java.name)
            .commit()
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}