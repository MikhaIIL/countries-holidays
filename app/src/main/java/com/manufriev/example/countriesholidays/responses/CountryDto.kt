package com.manufriev.example.countriesholidays.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class CountryDto (
    @JsonProperty("countryCode")
    val countryCode: String,
    @JsonProperty("name")
    val countryName: String)