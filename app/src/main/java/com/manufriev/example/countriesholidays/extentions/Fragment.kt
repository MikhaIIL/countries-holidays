package com.manufriev.example.countriesholidays.extentions

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

fun Fragment.isConnectionAvailable(): Boolean {
    val cm = requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val activeNetwork = cm.activeNetworkInfo
    return activeNetwork?.isConnected == true
}

fun Fragment.showToastLong(@StringRes stringId: Int) {
    Toast.makeText(context, stringId, Toast.LENGTH_LONG).show()
}