package com.manufriev.example.countriesholidays

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.manufriev.example.countriesholidays.features.country_info.CountryInfoViewModel
import com.manufriev.example.countriesholidays.features.favorites.FavoritesCountriesViewModel
import com.manufriev.example.countriesholidays.features.holidays.CountryHolidaysViewModel
import com.manufriev.example.countriesholidays.features.home_screen.HomeScreenViewModel
import com.manufriev.example.countriesholidays.repositories.HolidaysRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class ViewModelFactory(private val repository: HolidaysRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(HomeScreenViewModel::class.java) -> HomeScreenViewModel(repository) as T
            modelClass.isAssignableFrom(CountryInfoViewModel::class.java) -> CountryInfoViewModel(repository) as T
            modelClass.isAssignableFrom(FavoritesCountriesViewModel::class.java) -> FavoritesCountriesViewModel(repository) as T
            modelClass.isAssignableFrom(CountryHolidaysViewModel::class.java) -> CountryHolidaysViewModel(repository) as T
            else -> throw IllegalArgumentException("Unknown View Model class")
        }
    }

}