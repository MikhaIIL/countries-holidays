package com.manufriev.example.countriesholidays.datasources.cache

import com.manufriev.example.countriesholidays.db.CountriesDao
import com.manufriev.example.countriesholidays.datasources.entities.CountryEntity
import com.manufriev.example.countriesholidays.datasources.entities.CountryInfoEntity
import com.manufriev.example.countriesholidays.datasources.entities.FavoriteEntity
import com.manufriev.example.countriesholidays.datasources.entities.HolidayEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


class CountriesCacheDatasource(private val countriesDao: CountriesDao) {

    fun getCountries(): Flow<List<CountryEntity>> {
        return countriesDao.getCountries()
    }

    fun getCountries(codes: List<String>): List<CountryEntity> {
        return countriesDao.getCountries(codes)
    }

    fun insertCommonCountries(countries: List<CountryEntity>) {
        countriesDao.insertCommonCountries(countries)
    }

    fun getCountryInfo(countryCode: String): Flow<CountryInfoEntity?> {
        return countriesDao.getInfoByCountry(countryCode)
    }

    fun getCountriesInfo(countryCodes: List<String>): Flow<List<CountryInfoEntity>> {
        return countriesDao.getInfoByCountry(countryCodes)
    }

    fun insertInfo(entity: CountryInfoEntity) {
        countriesDao.insertInfo(entity)
    }

    fun getFavorites(): Flow<List<String>> {
        return countriesDao.getFavoritesCountries().map { it.map { it.code } }
    }

    fun changeFavorite(countryCode: String) {
        if (countriesDao.containsFavorite(countryCode)) {
            countriesDao.removeFromFavorite(countryCode)
        } else {
            countriesDao.addFavorite(FavoriteEntity(countryCode))
        }
    }

    fun getHolidays(code: String, year: Int): Flow<List<HolidayEntity>> {
        return countriesDao.getHolidays(code, year)
    }

    fun insertHolidays(entities: List<HolidayEntity>) {
        countriesDao.insertHolidays(entities)
    }
}